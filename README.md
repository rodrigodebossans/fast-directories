# Fast Directory Apliccation #

## Get Started ##

#### Set up environment variables ####

Create a ".env" file and following the directions in the ".env.example" file add your settings.

Example:

```
# DB ENVIRONMENTS
TYPEORM_CONNECTION=mysql
TYPEORM_HOST=db
TYPEORM_PORT=3306
TYPEORM_USERNAME=fast_directory
TYPEORM_PASSWORD=your-pass
TYPEORM_DATABASE=db_fast_directory
TYPEORM_SYNCHRONIZE=true
TYPEORM_LOGGING=true
TYPEORM_ENTITIES=src/app/model/**/*.ts
TYPEORM_MIGRATIONS=src/app/migration/**/*.ts
TYPEORM_SUBSCRIBERS=src/app/subscriber/**/*.ts
TYPEORM_ENTITIES_DIR=src/app/model
TYPEORM_MIGRATIONS_DIR=src/app/migration
TYPEORM_SUBSCRIBERS_DIR=src/app/subscriber
TYPEORM_DRIVER_EXTRA={"charset": "utf8mb4"}
SERVICE_DB_PORT=your-port-docker

# CORE ENVIRONMENTS
SERVICE_CORE_PORT=your-port-docker

```


After cloning this repository, in the root directory execute the command below and follow the instructions of each module

```bash
git submodule update --init --remote
```

##### Enjoy
